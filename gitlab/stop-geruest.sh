#!/bin/bash
set -e
set -x

GERUEST_REF="$CI_COMMIT_REF_SLUG"
GERUEST_HOSTNAME="$GERUEST_REF.$GERUEST_BASE_HOST"
GERUEST_PREFIX="/opt/domjudge/$GERUEST_REF"
GERUEST_DB_ADMIN_USER="admin"
GERUEST_DB_ADMIN_PASSWORD="CqMNWH3s7etj5oPp"

"$GERUEST_PREFIX"/domserver/bin/dj_setup_database -u "$GERUEST_DB_ADMIN_USER" -p "$GERUEST_DB_ADMIN_PASSWORD" uninstall
sudo rm /etc/nginx/sites-available/"$GERUEST_HOSTNAME" /etc/nginx/sites-enabled/"$GERUEST_HOSTNAME"
sudo systemctl reload nginx.service
sudo rm -r "$GERUEST_PREFIX"

