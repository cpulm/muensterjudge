#!/bin/bash
set -e
set -x

GERUEST_REF="$CI_COMMIT_REF_SLUG"
GERUEST_HOSTNAME="$GERUEST_REF.$GERUEST_BASE_HOST"
GERUEST_PREFIX="/opt/domjudge/$GERUEST_REF"
GERUEST_ADMIN_PASSWORD="pizzapizza"
GERUEST_DB_HOST="localhost"
GERUEST_DB_ADMIN_USER="admin"
GERUEST_DB_ADMIN_PASSWORD="CqMNWH3s7etj5oPp"
GERUEST_DB_USER="geruest_$GERUEST_REF"
GERUEST_DB_PASSWORD=$(head -c12 /dev/urandom | base64 | head -c16 | tr '/+' 'Aa')
GERUEST_DB_NAME="geruest_$GERUEST_REF"

# Configure and build DOMjudge
make dist
./configure --prefix="$GERUEST_PREFIX" --with-baseurl="https://$GERUEST_HOSTNAME/"
sudo make install-domserver
# we need to remove all files not owned by the gitlab-runner user, or else the next checkout will fail
sudo make clean

# Setup nginx
export GERUEST_REF
export GERUEST_HOSTNAME
# shellcheck disable=SC2016
envsubst '$GERUEST_REF,$GERUEST_HOSTNAME' < gitlab/nginx_template.conf | sudo tee /etc/nginx/sites-available/"$GERUEST_HOSTNAME"
sudo ln -s /etc/nginx/sites-available/"$GERUEST_HOSTNAME" /etc/nginx/sites-enabled/"$GERUEST_HOSTNAME"
sudo systemctl reload nginx.service

# Generate our own password files in order to specify a different DB username and a fixed admin password
# db_setup_database will not overwrite existing files, so this will work
(
	echo "# Randomly generated on host `hostname`, `date`"
	echo "# Format: 'dummy:<db_host>:<db_name>:<user>:<password>'"

	echo "dummy:$GERUEST_DB_HOST:$GERUEST_DB_NAME:$GERUEST_DB_USER:$GERUEST_DB_PASSWORD"
) > "$GERUEST_PREFIX"/domserver/etc/dbpasswords.secret
echo "$GERUEST_ADMIN_PASSWORD" > "$GERUEST_PREFIX"/domserver/etc/initial_admin_password.secret

"$GERUEST_PREFIX"/domserver/bin/dj_setup_database genpass
cat > ~/.netrc <<EOF
machine $GERUEST_HOSTNAME
login admin
password $GERUEST_ADMIN_PASSWORD

machine localhost
login admin
password $GERUEST_ADMIN_PASSWORD
EOF
"$GERUEST_PREFIX"/domserver/bin/dj_setup_database -u "$GERUEST_DB_ADMIN_USER" -p "$GERUEST_DB_ADMIN_PASSWORD" install
